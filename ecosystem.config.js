module.exports = {
  apps : [{
    name:'Find Stop/Idle ',
    script: './service/service_find_StopIdle/findStopIdle.js',    
    instances:1,
    autorestart:true,
    watch: true,
    max_memory_restart:'2G',
    env:{
      NODE_ENV:'development',
    },
    env_production:{
      NODE_ENV:'production',
    }
  },], 
};
