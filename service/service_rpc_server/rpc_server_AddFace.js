#!/usr/bin/env node
require('dotenv').config();
const axios = require('axios');
var qs = require('qs');
var amqp = require('amqplib/callback_api');
const url = require("url");
const NodeCache = require("node-cache");
const queryString = require('query-string');
const myCache = new NodeCache({ stdTTL: process.env.Expire_in }); // expire_in: 86399 ->24h

var amqpConn = null;
function start() {
    amqp.connect(process.env.StrConnectRabbitMQ, function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 5000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 5000);
        });

        console.log("[AMQP] connected");
        amqpConn = conn;
        amqpConn.createChannel(function (err, ch) {
            if (closeOnErr(err)) return;
            // channel.assertQueue(queue, {
            //     durable: true
            // });
            //ch.prefetch(10);
            console.log(' [x] Awaiting RPC requests');
            ch.consume("rpc_queue", function (msg) {
                work(msg, ch);
            }, { noAck: true });
        });
    });
}
function work(msg, ch) {
    var doc = JSON.parse(msg.content.toString());
    var url_img = process.env.Url_img;
    var fileId = process.env.FileId;
    let tokens = myCache.get('allTokens');
    if (tokens == null) {
        console.log("[x] cache token was expired at:", new Date().toLocaleTimeString());
        var data = {
            grant_type: process.env.GrantType,
            username: process.env.User,
            password: process.env.Password
        }
        axios({
            method: 'POST',
            url: process.env.APIPostToken,
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            data: qs.stringify(data),
        })
            .then(res => {
                myCache.set('allTokens', res.data.access_token, process.env.Expire_in);
                console.log("[x] new access_token: ", res.data.access_token);
                console.log("[x] Time start:", new Date().toLocaleTimeString());
                console.log(" [x] Params Received: ", JSON.stringify(doc));
                doc.picurl = encodeURIComponent(url_img + '/file?FileId=' + fileId + '&MimeType=image%2Fjpg&access_token=' + res.data.access_token);
                //doc.picurl=encodeURIComponent('https://dev.sovereignsolutions.com/fmis-backend-dev/api/v1/file?FileId='+fileId+'&MimeType=image%2Fjpg&access_token='+token);               
                console.log(" [x] return:");
                const params = new url.URLSearchParams(doc);
                //console.log(params);
                axios.get(process.env.APIPostAddDevice, { params })
                    .then((response1) => {
                        console.log(response1.data);
                        ch.sendToQueue(msg.properties.replyTo,
                            Buffer.from(JSON.stringify({ "hasError": response1.data.hasError, "message": response1.data.message })), {
                            correlationId: msg.properties.correlationId
                        });
                        console.log(' [x] continue awaiting rpc client requests:');
                    }).catch(error => {
                        console.log(error);
                    });

            })
            .catch((err) => {
                console.log("AXIOS ERROR: ", err);
            })
    }
    else {
        console.log("[x] cache access_token: ", tokens);
        console.log("[x] Time:", new Date().toLocaleTimeString());
        console.log("[x] Params Received: ", JSON.stringify(doc));
        doc.picurl = encodeURIComponent(url_img + '/file?FileId=' + fileId + '&MimeType=image%2Fjpg&access_token=' + tokens);

        console.log("[x] return:");
        const params = new url.URLSearchParams(doc);
        //console.log(params);
        axios.get(process.env.APIPostAddDevice, { params })
            .then((response1) => {
                console.log(response1.data);
                ch.sendToQueue(msg.properties.replyTo,
                    Buffer.from(JSON.stringify({ "hasError": response1.data.hasError, "message": response1.data.message })), {
                    correlationId: msg.properties.correlationId
                });
                console.log(' [x] continue awaiting rpc client requests:');
            }).catch(error => {
                console.log(error);
            });
    }
}
function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}
start();

