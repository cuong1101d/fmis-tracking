"use strict"
require('dotenv').config();
const http = require('http');
const queryString = require('query-string');
const map = {
  item: {
    deviceId: "uniqueid",
    driver: "name",
    //deviceMemo:"",
    //channels:['1','1'],
    status: "online",
    serverTime: "servertime",
    deviceTime: "devicetime",
    isValid: "valid",
    lat: "latitude",
    lng: "longitude",
    speed: "speed",
    heading: "course",
    mode: "",
    voltage: 0,
    sdCardHealth: ['-2', '-2'],
    manualDeviceCoordinate: "",
    isRegistered: true,
    //isRecording: true,
    //isTalking: false,
    timezone: 5.5,
    signalStrengthAndSatellites: 50,
    tag: "system"
  },
  operate: [
    {
      run: function (val) {

        return val.substr(0, 19);

      }, on: "serverTime"
    },
    {
      run: function (val) {

        return val.substr(0, 19);
      }, on: "deviceTime"
    },
    {

      run: function (val) {
        if (val = true) {
          return 1;
        } else {
          return 0;
        }
      }
      , on: "isValid"
    },
    {
      run: function (val) {

        return "online";

      }
      , on: "status"
    },
    {
      run: function (val) {
        if (val = true) {
          return 1;
        } else {
          return 0;
        }
      }
      , on: "isRegistered"
    },
  ],
};
const map1 = {
  item: {
    serialno: "id",
    system: "system",
    deviceid: "uniqueid",
    eventtime: "eventtime",
    fenceid: "geofenceid",
    vehiclex: "latitude",
    vehicley: "longitude",
    updatetime: "eventtime",
    speed: "speed",
    appdevid: "type"
  },
  operate: [
    {
      run: function (val1) {

        return val1.substr(0, 19);
      }, on: "eventtime"
    },
    {
      run: function (val1) {

        return val1.substr(0, 19);
      }, on: "updatetime"
    }
  ],
};
// if the connection is closed or fails to be established at all, we will reconnect
var amqp = require('amqplib/callback_api');
var { transform } = require("node-json-transform");
var amqpConn = null;
function start() {
  amqp.connect(process.env.StrConnectRabbitMQ, function (err, conn) {
    if (err) {
      console.error("[AMQP]", err.message);
      return setTimeout(start, 5000);
    }
    conn.on("error", function (err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function () {
      console.error("[AMQP] reconnecting");
      return setTimeout(start, 1000);
    });

    console.log("[AMQP] connected");
    amqpConn = conn;

    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}
// A worker that acks messages only if processed succesfully
function startWorker() {
  amqpConn.createChannel(function (err, ch) {
    if (closeOnErr(err)) return;
    ch.consume("tc_events", processMsg, { noAck: false });
    console.log("Worker 1 is started - queue: tc_events");

    ch.consume("tc_positions", processMsg1, { noAck: false });
    console.log("Worker 2 is started - - queue: tc_positions");
    ch.on("close", function () {
      console.log("[AMQP] channel closed");
    });

    function processMsg(msg) {
      work(msg, function (ok) {
        try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        } catch (e) {
          closeOnErr(e);
        }
      });
    }
    function processMsg1(msg) {
      work1(msg, function (ok) {
        try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        } catch (e) {
          closeOnErr(e);
        }
      });
    }
  });
}

function work(msg, cb) {
  //console.log("Got msg", msg.content.toString());
  const doc = JSON.parse(msg.content.toString());
  var query_insert = "";
  //var result1 = JSON.stringify(transform(doc, map1));
  var result = transform(doc, map1);
  // query_insert = "INSERT INTO fmis.GKEvent  FORMAT JSONEachRow " + result.toString();
  var id_events_type = " coalesce(GetID_type('" + result.appdevid + "'), 0) ";
  query_insert = "INSERT INTO fmis.GKEvent (serialno,system,deviceid,appdevid,eventtime,updatetime,fenceid,vehiclex,vehicley,speed) select " +
    result.serialno + ",'" + result.system + "','" + result.deviceid + "'," + id_events_type + ",'" + result.eventtime + "','" + result.updatetime + "'," + result.fenceid + "," + result.vehiclex + "," + result.vehicley + "," + result.speed;

  console.log("[x] Transform data json GKEvent: %s", JSON.stringify(result));
  console.log(" ------------------------------End data insert GKEvent-----------------------------------");
  const options = {
    'method': 'POST',
    'path': '/?' + queryString.stringify({
      'database': process.env.DB_NAME,
      'query': query_insert,
    }),
    'port': process.env.DB_PORT,
    'hostname': process.env.DB_HOST,    
    'headers': {
      'X-ClickHouse-User': process.env.DB_USER,
      'X-ClickHouse-Key': process.env.DB_PASS,
    },
  };
  const rs = http.request(options, (res) => {
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      console.log(chunk);
    });
  });
  rs.end();
  cb(true);
}
function work1(msg, cb) {
  //console.log("Got msg", msg.content.toString());
  const doc = JSON.parse(msg.content.toString());
  var query_insert = "";
  var result = JSON.stringify(transform(doc, map));
  query_insert = "INSERT INTO fmis.GPS FORMAT JSONEachRow " + result.toString();
  console.log("[x] Transform data json GPS: %s", result);
  console.log(" ------------------------------End data insert GPS-----------------------------------");
  const options = {
    'method': 'POST',
    'path': '/?' + queryString.stringify({
      'database': process.env.DB_NAME,
      'query': query_insert,
    }),
    'port': process.env.DB_PORT,
    'hostname': process.env.DB_HOST,
    //'body':result,
    'headers': {
      'X-ClickHouse-User': process.env.DB_USER,
      'X-ClickHouse-Key': process.env.DB_PASS,
    },
  };
  const rs = http.request(options, (res) => {
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      console.log(chunk);
    });
  });
  rs.end();

  cb(true);
}
function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] error", err);
  amqpConn.close();
  return true;
}
start();