#!/usr/bin/env node
require('dotenv').config();
const config = require('../../config/environment.js');
const settingTracking = require('../../config/settingTracking.js')
const Mongodb = require('../../config/connectMongoDb.js')
const clickhouse = require('../../config/connectClickHouse.js')
var amqp = require('amqplib/callback_api');
const NodeCache = require("node-cache");
const myCache = new NodeCache({ checkperiod: config.myCache.alldata_expire });// will check every 60 minute
function Converttimestamp(strDate) {
    var datum = Date.parse(strDate);
    return datum / 1000;
}
var amqpConn = null;
function start() {
    amqp.connect(process.env.StrConnectRabbitMQ, function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 5000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 5000);
        });

        console.log("[AMQP] connected");
        amqpConn = conn;
        amqpConn.createChannel(async function (err, ch) {
            if (closeOnErr(err)) return;
            // channel.assertQueue(queue, {
            //     durable: true
            // });
            //ch.prefetch(10); 
            var dictSettingTracking = {};
            var dictdataIDLE = {};
            var dictIDLESignal = {};
            var dictRunningSignal = {};
            var dictdataSTOP = {};
            console.log(' [x] Awaiting Car requests:');
            await settingTracking.initFuncionTracking(process.env.SYSTEM_TRACKING).then(dataSetting => {
                dictSettingTracking = dataSetting;
            })
            await clickhouse.initDictData(dictSettingTracking, dictdataSTOP, dictdataIDLE)
            ch.consume(process.env.QueueGPS, async function (msg) {
                var doc = JSON.parse(msg.content.toString());
                console.log(' [x] Awaiting Car requests:', doc.gps);
                console.log(' [x] deviceid:', doc.deviceid);
                // check IDLE Event
                if (dictdataIDLE[doc.deviceid]) // already has data
                {
                    await clickhouse.getSysId(dictSettingTracking, doc.deviceid).then(sysId => {
                        console.log("sysId:", sysId);
                        processCheckIDLE(dictdataIDLE[doc.deviceid], doc, ch, dictdataIDLE,dictSettingTracking, dictIDLESignal, dictRunningSignal, sysId)
                    });
                }
                else  dictdataIDLE[doc.deviceid] = doc
               
                // check STOP Event
                if (dictdataSTOP[doc.deviceid]) // already has data
                {
                    await clickhouse.getSysId(dictSettingTracking,doc.deviceid).then(sysId => {
                        console.log("sysId:", sysId);
                        processCheckSTOP(dictdataSTOP[doc.deviceid], doc, ch, dictdataSTOP,dictSettingTracking, sysId);
                    });
                }
                else dictdataSTOP[doc.deviceid] = doc

            }, { noAck: true });

        });
    });
}

async function processCheckIDLE(dataold, datanew, ch, dictdata,dictSettingTracking, dictIDLESignal, dictRunningSignal, SysId) {
    // check dataold - init table v_WayPoint_Lastseen   
    var GPSold = new Object();
    console.log("Check Idle event of location:",dataold.gps)
    if (dataold.gps != undefined) {
        GPSold = getLocation(dataold.gps);
        //console.log("rabbitmq:",GPSold)    
    }
    else {
        let timec = dataold.timestamp.toString().replace(' ', 'T') + ".000Z";
        GPSold =
        {
            time: timec,
            lng: dataold.lng,
            lat: dataold.lat,
            speed: dataold.speed
        }
        //console.log("table v_WayPoint_Lastseen",GPSold)  
    }
    var GPSnew = getLocation(datanew.gps);
    var StartTime = new Date(GPSold.time);
    var ArrivalTime = new Date(GPSnew.time);
    const driver = datanew.driver;

    var configIdle = {
        source: config.event.idle.source,
        eventType: config.event.idle.name,
        typeId: config.event.idle.typeId,
        speed: config.event.idle.speed,
        numberOfIDLESignal: config.event.idle.numberOfIDLESignal,
        numberOfRunningSignal: config.event.idle.numberOfRunningSignal,
        distance: config.event.idle.distance
    }
    var EventData = {
        TypeId: null,
        EventType: "",
        Source: configIdle.source,
        DeviceCode: datanew.deviceid,
        Driver: driver,
        lng_start: GPSold.lng,
        lat_start: GPSold.lat,
        Speed: null,
        GPS1: [GPSold.lng, GPSold.lat],
        GPS2: [GPSnew.lng, GPSnew.lat],
        Meta: "Json",
        StartTime: StartTime,
        ArrivalTime: ArrivalTime,
        Duration: null,
        Distance: null,
        SysId: ""
    };
    EventData.Duration = new Date(GPSnew.time).getTime() - new Date(GPSold.time).getTime();
    console.log("duration (seconds):", EventData.Duration / 1000);
    console.log("new speed m/s:", GPSnew.speed);
    console.log("old speed m/s:", GPSold.speed);
    try {
        let tokens = myCache.get('idleEvent');
        if (tokens == null) {
            console.log("[x] cache token idleEvent was expired at:", new Date().toLocaleTimeString());
            await Mongodb.getConfigAll(dictSettingTracking).then(res => {
                myCache.set('idleEvent', res, config.myCache.stopidle_expire); // expire_in 1 minutes               
                if (res[SysId] != undefined) {
                    configIdle.speed = res[SysId].idle.speed;
                    configIdle.numberOfIDLESignal = res[SysId].idle.numberOfIDLESignal;
                    configIdle.numberOfRunningSignal = res[SysId].idle.numberOfRunningSignal;
                    configIdle.distance = res[SysId].idle.distance;
                }
            }) 
        }
        else {

            if (tokens[SysId] != undefined) {
                configIdle.speed = tokens[SysId].idle.speed;
                configIdle.numberOfIDLESignal = tokens[SysId].idle.numberOfIDLESignal;
                configIdle.numberOfRunningSignal = tokens[SysId].idle.numberOfRunningSignal;
                configIdle.distance = tokens[SysId].idle.distance;
            }
            
        }

        // check event IDLE 
        if (GPSold.speed < configIdle.speed && GPSnew.speed < configIdle.speed) {
            //vẫn đang trong trạng thái IDLE , tiếp tục kiểm tra tiếp 
            console.log("continue check gps_end m/s : ", GPSold.speed)

            if (dictIDLESignal[GPSnew.deviceid]) // already has data
            {
                dictIDLESignal[GPSnew.deviceid] += 1;
            }
            else
                dictIDLESignal[GPSnew.deviceid] = 1;
        }
        else if (GPSold.speed > configIdle.speed && GPSnew.speed > configIdle.speed) {
            //chuyển trạng thái IDLE >3 , tiếp tục kiểm tra di chuyển .  
            console.log("continue check gps_end m/s : ", GPSold.speed)

            if (dictRunningSignal[GPSnew.deviceid]) // already has data
            {
                dictRunningSignal[GPSnew.deviceid] += 1;
            }
            else
                dictRunningSignal[GPSnew.deviceid] = 1;
        }
        else if (GPSold.speed < configIdle.speed && GPSnew.speed > configIdle.speed) {
            //save to queue cuongdd.test
            EventData.Speed = GPSold.speed;
            EventData.StartTime = new Date(GPSold.time),
                EventData.ArrivalTime = new Date(GPSnew.time),
                EventData.Duration = new Date(GPSnew.time).getTime() - new Date(GPSold.time).getTime(),
                EventData.lng_start = GPSold.lng,
                EventData.lat_start = GPSold.lat,
                EventData.GPS1 = [GPSold.lng, GPSold.lat],
                EventData.GPS2 = [GPSnew.lng, GPSnew.lat],
                EventData.Distance = distance(GPSold.lng, GPSold.lat, GPSnew.lng, GPSnew.lat) * 1000;
            EventData.Meta = { "distance": EventData.Distance, "duration": EventData.Duration / 1000, "lng_end": GPSnew.lng, "lat_end": GPSnew.lat };
            console.log("IDLE event GPS_new speed >0.28 m/s and distance: ", EventData.Distance)
            dictdata[datanew.deviceid] = datanew;
            // kiểm tra 4 đk IDLE event 
            if (dictRunningSignal[GPSnew.deviceid] >= configIdle.numberOfRunningSignal && dictIDLESignal[GPSnew.deviceid] >= configIdle.numberOfIDLESignal && EventData.Distance > configIdle.distance && EventData.Speed <= configIdle.speed) {
                EventData.EventType = configIdle.eventType;
                EventData.TypeId = configIdle.typeId;
                EventData.SysId = SysId;
                EventData.Duration = EventData.Duration / 1000

                ch.sendToQueue(process.env.QueueEvent, Buffer.from(JSON.stringify(EventData)));
                // reset again param 
                dictRunningSignal[GPSnew.deviceid] = 0;
                dictIDLESignal[GPSnew.deviceid] = 0;
            }
        }        
        else
        console.log("4 Condition IDLE event:",dictRunningSignal[GPSnew.deviceid],">=",configIdle.numberOfRunningSignal, dictIDLESignal[GPSnew.deviceid],">=" ,configIdle.numberOfIDLESignal," distance:", EventData.Distance,">" ,configIdle.distance/1000, "speed:",EventData.Speed,"<=" ,configIdle.speed)

    } catch (error) {
        console.log("error:", error);
    }

}
async function processCheckSTOP(dataold, datanew, ch, dictdata,dictSettingTracking, SysId) {
    // check dataold - init table v_WayPoint_Lastseen   
    var GPSold = new Object();
    console.log("Check Stop event of location:",dataold.gps)
    if (dataold.gps != undefined) {
        GPSold = getLocation(dataold.gps);
        //console.log("rabbitmq:",GPSold)    
    }
    else {
        let timec = dataold.timestamp.toString().replace(' ', 'T') + ".000Z";
        GPSold =
        {
            time: timec,
            lng: dataold.lng,
            lat: dataold.lat,
            speed: dataold.speed
        }
        //console.log("table v_WayPoint_Lastseen",GPSold)  
    }
    var GPSnew = getLocation(datanew.gps);
    var StartTime = new Date(GPSold.time);
    var ArrivalTime = new Date(GPSnew.time);
    const driver = datanew.driver;
    var configStop = {
        source: config.event.stop.source,
        eventType: config.event.stop.name,
        typeId: config.event.stop.typeId,
        waitingTime: config.event.stop.waitingTime,
        distance: config.event.stop.distance
    }
    var EventData = {
        TypeId: configStop.typeId,
        EventType: configStop.eventType,
        Source: configStop.source,
        DeviceCode: datanew.deviceid,
        Driver: driver,
        lng_start: GPSold.lng,
        lat_start: GPSold.lat,
        Speed: null,
        GPS1: [GPSold.lng, GPSold.lat],
        GPS2: [GPSnew.lng, GPSnew.lat],
        Meta: "Json",
        StartTime: StartTime,
        ArrivalTime: ArrivalTime,
        Duration: null,
        Distance: null, //(3.503 km)
        SysId: ""
    };
    EventData.Duration = (new Date(GPSnew.time).getTime() - new Date(GPSold.time).getTime()) / 1000;
    console.log("Duration (seconds):", EventData.Duration);
    console.log("Start time:", StartTime);
    console.log("Arrival time:", ArrivalTime);
    try {
        let tokens = myCache.get('stopEvent');
        // original source and store it in the cache
        if (tokens == null) {
            console.log("[x] cache token stopEvent  was expired at:", new Date().toLocaleTimeString());
            await Mongodb.getConfigAll(dictSettingTracking).then(res => {
                myCache.set('stopEvent', res, config.myCache.stopidle_expire); // expire_in 1 minutes               
                if (res[SysId] != undefined) {
                    configStop.waitingTime = res[SysId].stopEvent.waitingTime;
                    configStop.distance = res[SysId].stopEvent.distance;
                }
            })         
        }
        else {
            //Tokens convert json     
            if (tokens[SysId] != undefined) {
                configStop.waitingTime = tokens[SysId].stopEvent.waitingTime;
                configStop.distance = tokens[SysId].stopEvent.distance;
            }
        }
        // check event STOP 
        if (ArrivalTime.getTime() - StartTime.getTime() >= configStop.waitingTime) //5mintutes
        {
            var kq = distance(GPSold.lng, GPSold.lat, GPSnew.lng, GPSnew.lat)
            if (kq < configStop.distance / 1000)  //status changed 
            {
                console.log("Event stop:",EventData.DeviceCode);
                EventData.SysId = SysId;
                EventData.EventType = configStop.eventType;
                EventData.TypeId = configStop.typeId;
                EventData.Speed = GPSnew.speed;
                EventData.Distance = kq * 1000;
                EventData.Meta = { "distance": EventData.Distance, "duration": EventData.Duration, "lng_end": GPSnew.lng, "lat_end": GPSnew.lat };                
                ch.sendToQueue(process.env.QueueEvent, Buffer.from(JSON.stringify(EventData)));
            }
        }
        dictdata[datanew.deviceid] = datanew;
    } catch (error) {
        console.log("error:", error);
    }
}
function getLocation(gprmc) {
    const speedFactor = parseFloat('0.514444f');   // knot -> m/s   
    //var gprmc= '$GPRMC,002047.000,A,2257.3901,N,08810.3704,E,35.9,146.3,140822,,,A*55'
    var gpsArr = gprmc.split(',');
    if (gpsArr.length < 10) {

        console.log("format gprmc error");
        return false;
    }
    var deviceTimeStr = gpsArr[1];
    var hours = gpsArr[1].slice(0, 2);
    var minutes = gpsArr[1].slice(2, 4);
    var seconds = gpsArr[1].slice(4, 6);
    var deviceDateStr = gpsArr[9];
    let yy = new Date().getFullYear().toString().slice(0, 2);
    var year = yy + gpsArr[9].slice(4, 6)
    var month = gpsArr[9].slice(2, 4)
    var day = gpsArr[9].slice(0, 2)
    var GPSInfo =
    {
        lat: ParseCoordValue(gpsArr[3], gpsArr[4]),
        lng: ParseCoordValue(gpsArr[5], gpsArr[6]),
        speed: parseFloat(gpsArr[7]) * speedFactor,
        heading: parseFloat(gpsArr[8]),
        isValid: gpsArr[2] == "A",
        time: new Date(year.toString() + "-" + month.toString() + "-" + day.toString() + "T" + hours.toString() + ":" + minutes.toString() + ":" + seconds.toString() + "Z"),
        timestamp: Converttimestamp(new Date(year.toString() + "-" + month.toString() + "-" + day.toString() + "T" + hours.toString() + ":" + minutes.toString() + ":" + seconds.toString() + "Z"))
    }  
    return GPSInfo;
}
function ParseCoordValue(value, direction) {
    var v = parseFloat(value);
    v = (Math.floor(v / 100) + ((v / 100 - Math.floor(v / 100)) / 0.6));
    if (direction == "S" || direction == "W")
        v = -1 * v;
    return v;
}
function distance(xBegin, yBegin, xEnd, yEnd) {
    var r = Math.PI / 180.0;
    var dbLat1InRad = yBegin * r;
    var dbLat2InRad = yEnd * r;

    var dbTheta = xBegin - xEnd;
    var dbThetaInRad = dbTheta * r;

    var a = Math.sin(dbLat1InRad);
    var b = Math.sin(dbLat2InRad);
    var c = Math.cos(dbLat1InRad);
    var d = Math.cos(dbLat2InRad);
    var e = Math.cos(dbThetaInRad);
    var f = a * b + c * d * e;

    if (f > 1)
        f = 1;
    if (f < -1)
        f = -1;

    var dbDistInRad = Math.acos(f);
    var dbDist = dbDistInRad * 180.0 / Math.PI;
    dbDist = dbDist * 60.0 * 1.1515;
    dbDist = dbDist * 1.609344; // *1000; trả về km

    return dbDist;
}
function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}
start()
