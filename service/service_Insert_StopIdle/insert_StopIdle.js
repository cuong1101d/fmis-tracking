"use strict"
require('dotenv').config();
const http = require('http');
const queryString = require('query-string');
const map = {
  item: {
    typeId: "TypeId",
    event_type: "EventType", 
    source:"Source",
    driver: "Driver",
    device_code: "DeviceCode",    
    lng_start:"lng_start",
    lat_start:"lat_start",
    speed:"Speed",
    start_time: "StartTime",
    arrival_time: "ArrivalTime",
    meta:"Meta",
    gps_start: "GPS1",
    gps_end: "GPS2",
    distance:"Distance",
    duration:"Duration",
    sysId: "SysId",
  },
  operate: [
    {
      run: function (val) {

        return val.substr(0, 19);

      }, on: "start_time"
    },
    {
      run: function (val) {

        return val.substr(0, 19);
      }, on: "arrival_time"
    },  
  
  ],
};

// if the connection is closed or fails to be established at all, we will reconnect
var amqp = require('amqplib/callback_api');
var { transform } = require("node-json-transform");
var amqpConn = null;
function start() {
  amqp.connect(process.env.StrConnectRabbitMQ, function (err, conn) {
    if (err) {
      console.error("[AMQP]", err.message);
      return setTimeout(start, 5000);
    }
    conn.on("error", function (err) {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });
    conn.on("close", function () {
      console.error("[AMQP] reconnecting");
      return setTimeout(start, 1000);
    });

    console.log("[AMQP] connected");
    amqpConn = conn;

    whenConnected();
  });
}

function whenConnected() {
  startWorker();
}
// A worker that acks messages only if processed succesfully
function startWorker() {
  amqpConn.createChannel(function (err, ch) {
    if (closeOnErr(err)) return;
    ch.consume(process.env.QueueEvent, processMsg, { noAck: false });
    console.log("Worker 1 is started - queue:",process.env.QueueEvent);
    ch.on("close", function () {
      console.log("[AMQP] channel closed");
    });

    function processMsg(msg) {
      work(msg, function (ok) {
        try {
          if (ok)
            ch.ack(msg);
          else
            ch.reject(msg, true);
        } catch (e) {
          closeOnErr(e);
        }
      });
    }   
  });
}


function work(msg, cb) {
  //console.log("Got msg", msg.content.toString());
  const doc = JSON.parse(msg.content.toString());
  var query_insert = "";
  //var result = JSON.stringify(transform(doc, map));
  var result = transform(doc, map);
  // query_insert = "INSERT INTO fmis.GKEvent  FORMAT JSONEachRow " + result.toString();  

  var meta=JSON.stringify(result.meta);  
  console.log(" [x] Find Event: %s", result.event_type);
  console.log( "[x] duration: %s", result.meta.duration);
  //console.log(" [x] meta: %s", JSON.stringify(result.meta));
  query_insert = "INSERT INTO fmis.CalculatedEvent (typeId,event_type,source,driver,device_code,start_time,arrival_time,lat_start,lng_start,speed,meta,gps_start,gps_end,distance,duration,sysId) select " +result.typeId+",'"+
  result.event_type + "',"+result.source +",'"+ result.driver + "','" + result.device_code + "','" + result.start_time + "','" + result.arrival_time + "',"+result.lat_start+","+result.lng_start +","+result.speed+",'"+meta+"','"+result.gps_start + "','" + result.gps_end + "',"+ result.distance+","+result.meta.duration+",'"+result.sysId+"'";

 // query_insert = "INSERT INTO db_cuongdd.Event FORMAT JSONEachRow " + result.toString();
 //console.log("[x] Query insert: %s", query_insert); 
  console.log(" ------------------------------End data insert STOP/IDLE Event -----------------------------------");
  const options = {
    'method': 'POST',
    'path': '/?' + queryString.stringify({
      'database': process.env.DB_NAME,
      'query': query_insert,
    }),
    'port': process.env.DB_PORT,
    'hostname': process.env.DB_HOST,
    //'body':result,
    'headers': {
      'X-ClickHouse-User': process.env.DB_USER,
      'X-ClickHouse-Key': process.env.DB_PASS,
    },
  };
  const rs = http.request(options, (res) => {
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      console.log(chunk);
    });
  });
  rs.end();

  cb(true);
}
function closeOnErr(err) {
  if (!err) return false;
  console.error("[AMQP] error", err);
  amqpConn.close();
  return true;
}
start();