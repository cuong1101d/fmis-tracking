#!/usr/bin/env node
'use strict';
require('dotenv').config();
const express = require("express");
const { Worker } = require("worker_threads");
const app = express();
const port = process.env.PORT || 3000;
const clickhouse = require('../../config/connectClickHouse.js')
const THREAD_COUNT = process.env.CPU_CORE * 2 + 1;
function createWorker(datachunk1, i1, len1) {
    return new Promise(function (resolve, reject) {
        const worker = new Worker("./service/service_find_distance/workerCPU.js", {
            workerData: { thread_count: THREAD_COUNT, datachunk: datachunk1, i: i1, len: len1 },
        });
        worker.on("message", (data) => {
            resolve(data);
        });
        worker.on("error", (msg) => {
            reject(`An error ocurred: ${msg}`);
        });
    });
}
app.get(process.env.APIFindDistance, async (req, res) => {
    try {
        var fromTime = req.query["fromTime"];
        var toTime = req.query["toTime"];
        var deviceId = req.query["deviceId"];
        var totalDistance = 0;
        var strquery1 = "select deviceId,lat,lng,deviceTime FROM fmis.GPS where deviceId ={Id:String} and deviceTime between toDateTime({from:UInt32}) and toDateTime({to:UInt32}) order by deviceTime ASC"
        //console.log(strquery1);
        const rows = await clickhouse.query(strquery1,
            {
                params: {
                    Id: deviceId,
                    from: fromTime,
                    to: toTime
                },
            }).toPromise();
        let dt = JSON.parse(rows).data;
        var chunkdt = 0;
        console.log("total record:", dt.length);
        if (dt.length >= THREAD_COUNT * 2) {
            //Mutil_thread
            chunkdt = Math.ceil(dt.length / THREAD_COUNT);
        }
        else if (dt.length < THREAD_COUNT * 2 && dt.length > 2) {
            // 1 thread
            chunkdt = dt.length;
        }
        else {
            console.log("Have one Coordinates doesnt find distance ");
            res.status(500).send("Coordinates doesnt find distance ");
        }
        var tempArr = [];
        var chunk = chunkdt; //The chunk size you want
        if (dt.length <= chunk) {
            tempArr.push({ data: dt, i: 0, count: dt.length });
        }
        else {
            for (let index = 0; index < dt.length; index = index + chunk) {
                //const element = dt.data[index];     
                var len = chunk;
                if (index + chunk < dt.length) {
                    len = index + chunk;
                    tempArr.push({ data: dt, i: index, count: len });
                }
                else {
                    if ((index + chunk) - dt.length == chunk - 1) {
                        //console.log("Have one Coordinates doesnt find distance ");
                        tempArr[tempArr.length - 1].count = tempArr[tempArr.length - 1].count + 1;
                    }
                    else {
                        len = dt.length;
                        tempArr.push({ data: dt, i: index, count: len });
                    }

                }

            }
        }

        //console.log("count data segment:", tempArr.length);
        const workerPromises = [];
        for (let index = 0; index < tempArr.length; index++) {
            const element = tempArr[index];
            workerPromises.push(createWorker(element.data, element.i, element.count));
        }
        Promise.all(workerPromises)
            .then((result) => {
                result.forEach(element1 => {
                    totalDistance = totalDistance + element1;
                });
                console.log("data segment:", result);
                console.log("Total distance:", totalDistance);
                res.json({ "DataSegment:": result.length, "TotalDistance:": totalDistance });
            })
            .catch((e) => {
                // Handle errors here
                console.log("error", e);
            });

    } catch (error) {
        console.log(error);
    }
});
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});