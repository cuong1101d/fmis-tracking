'use strict';
require('dotenv').config();
const { workerData, parentPort } = require("worker_threads");
var OSRM = require('../../config/osrm');
var osrm = new OSRM(process.env.OSRM_SERVER);
function Converttimestamp(strDate) {
    var datum = Date.parse(strDate);
    return datum / 1000;
}
async function getMatch({ datachunk, i, len }) {
    var tempArr = [];
    var chunk = 50; //The chunk size you want
    if (len <= chunk) {
        tempArr.push({ data: datachunk, i: 0, count: len });
    }
    else {
        for (let index = i; index < len; index = index + chunk) {
            //const element = dt.data[index];     
            var len1 = chunk;
            if (index + chunk < len) {
                len1 = index + chunk;
                tempArr.push({ data: datachunk, i: index, count: len1 });
            }
            else {
                if ((index + chunk) - len == chunk - 1) {
                    //console.log("Have one Coordinates doesnt find distance ");
                    tempArr[tempArr.length - 1].count = tempArr[tempArr.length - 1].count + 1;
                }
                else {
                    len1 = len;
                    tempArr.push({ data: datachunk, i: index, count: len1 });
                }

            }

        }
    }

    console.log("count data segment:", tempArr);
    var promises = [];
    for (let index = 0; index < tempArr.length; index++) {
        const element = tempArr[index];
        promises.push(osrmMatch({ data: element.data, i: element.i, len: element.count }));
    }
    try {
        return new Promise((res, rej) => {
            // call n osrm 
            var totalDistance = 0;
            Promise.all(promises)
                .then((result) => {
                    // if (err) rej(err)
                    result.forEach(element1 => {
                        totalDistance = totalDistance + element1;
                    }
                    );

                    res(totalDistance);
                })
                .catch((e) => {
                    // Handle errors here
                    console.log("error", e);
                });
        })
    } catch (err) {
        console.log(err);
        return 0;
    }
}
function osrmMatch({ data, i, len }) {
    var traceCoordinates = [];
    var traceTimestamps = [];
    var traceRadiuses = [];
    for (let index = i; index < len; index++) {

        traceCoordinates.push([data[index].lng, data[index].lat]);
        traceTimestamps.push(Converttimestamp(data[index].deviceTime));
        traceRadiuses.push(49); // R: 25m
    }
    //console.log(traceCoordinates);

    try {
        return new Promise((res, rej) => {
            // call n osrm 
            osrm.match({
                //profile:['car'],
                coordinates: traceCoordinates,
                timestamps: traceTimestamps,
                steps: true,
                overview: 'full',
                geometries: 'geojson',
                annotations: true,
                radiuses: traceRadiuses,
            }, function (err, result) {
                if (err) rej(err)
                //let response = result.matchings[0].geometry;
                //console.log("match geojson: ", JSON.stringify(response));
                res(result.matchings[0].distance);
            });
        })
    } catch (err) {
        console.log(err);
        return 0;
    }
}
getMatch({ datachunk: workerData.datachunk, i: workerData.i, len: workerData.len }).then(data => {

    parentPort.postMessage(data);
});

