#!/usr/bin/env node
require('dotenv').config();
const axios = require('axios');
var qs = require('qs');
var amqp = require('amqplib/callback_api');
var { transform } = require("node-json-transform");
const NodeCache = require("node-cache");
const myCache = new NodeCache({ stdTTL: process.env.Expire_in }); // expire in: 15 mintutes expire_in: 86399
const map = {
    item: {
        device_code: "deviceid",
        device_type_id: [
            "GPS", "Camera"
        ],
        hosting_id: "net_server",
        device_provider_id: "servertime",
        device_phone: "devicetime",
        device_number_channel: "channel",
        device_mobile_isp_id: "latitude",
        device_registered_on: "updatetime",
        device_os_name: "speed",
        integration_type_id: "course",
    },
};
//GetToken(process.env.GrantType, process.env.User, process.env.Password);
function GetToken(grant_type1, username1, password1) {
    try {
        let tokens = myCache.get('allTokens');
        console.log("[x] node-cache token:", tokens);
        // if posts does not exist in the cache, retrieve it from the
        // original source and store it in the cache
        if (tokens == null) {
            var data = {
                grant_type: grant_type1,
                username: username1,
                password: password1
            }
            axios({
                method: 'POST',
                url: process.env.APIPostToken,
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                },
                data: qs.stringify(data),
            })
                .then(res => {
                    // and the next request will hit the API again
                    myCache.set('allTokens', res.data.access_token, 300);
                    console.log("[x] new access_token: ", res.data.access_token);
                    //start(res.data.access_token);
                })
                .catch((err) => {
                    console.log("AXIOS ERROR: ", err);
                })
        }
        else {
            start(tokens);
        }

    } catch (error) {
        return setTimeout(GetToken(process.env.GrantType, process.env.User, process.env.Password), 5000);
    }

}
var amqpConn = null;
function start() {
    amqp.connect(process.env.StrConnectRabbitMQ, function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 5000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 5000);
        });

        console.log("[AMQP] connected");
        amqpConn = conn;
        amqpConn.createChannel(function (err, ch) {
            if (closeOnErr(err)) return;
            // channel.assertQueue(queue, {
            //     durable: true
            // });
            //ch.prefetch(10);
            console.log(' [x] Awaiting Car requests:');
            ch.consume("car", function (msg) {
                console.log(" [x] routingKey Received: ", msg.fields.routingKey);
                work(msg);

            }, { noAck: true });
        });
    });
}
function work(msg) {
    var doc = JSON.parse(msg.content.toString());
    console.log("data:",JSON.stringify(doc));
    var result = transform(doc, map);
    var str = result.hosting_id.split(':');
    var hosting_new;
    if (str==null) {
        hosting_new=result.hosting_id;
    }
    else
    {
        hosting_new=str[0];
    }
    var data1 = {
        device_code: result.device_code.trimEnd(),
        device_type_id: [
            "GPS", "Camera"
        ],
        hosting_id: hosting_new,
        //device_provider_id:null,
        //device_phone: "+919999999999",
        device_number_channel: result.device_number_channel,
        //device_mobile_isp_id: null,
        device_registered_on: result.device_registered_on,
        //device_os_name: null,
        integration_type_id: "Fuho"
    }

    let tokens = myCache.get('allTokens');
    //console.log("[x] node-cache token:", tokens);
    // if posts does not exist in the cache, retrieve it from the
    // original source and store it in the cache
    if (tokens == null) {
        console.log("[x] cache token was expired at:", new Date().toLocaleTimeString());
        var data = {
            grant_type: process.env.GrantType,
            username: process.env.User,
            password: process.env.Password
        }
        axios({
            method: 'POST',
            url: process.env.APIPostToken,
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            data: qs.stringify(data),
        })
            .then(res => {
                // and the next request will hit the API again
                myCache.set('allTokens', res.data.access_token, process.env.Expire_in);
                console.log("[x] new access_token: ", res.data.access_token);
                console.log("[x] Time:", new Date().toLocaleTimeString());
                console.log("[x] param input:", data1);
                let axiosConfig = {
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        "Access-Control-Allow-Origin": "*",
                        'Authorization': `bearer ${res.data.access_token}`
                    }
                };
                axios.post(process.env.APIPostAddDevice, data1, axiosConfig)
                    .then((res) => {
                        console.log("RESPONSE RECEIVED: ", res.data);
                        console.log(' [x] continue awaiting Car requests:');
                    })
                    .catch((err) => {
                        console.log("AXIOS ERROR: ", err);
                    })
            })
            .catch((err) => {
                console.log("AXIOS ERROR: ", err);
            })
    }
    else {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
                'Authorization': `bearer ${tokens}`
            }
        };
        console.log("[x] cache access_token: ", tokens);
        console.log("[x] Time:", new Date().toLocaleTimeString());
        axios.post(process.env.APIPostAddDevice, data1, axiosConfig)
            .then((res) => {
                console.log("RESPONSE RECEIVED: ", res.data);
                console.log(' [x] continue awaiting Car requests:');
            })
            .catch((err) => {
                console.log("AXIOS ERROR: ", err);
            })
    }

}

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}
start();
