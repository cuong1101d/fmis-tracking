'use strict';
require('dotenv').config();
const setting = require('./environment.js');
const { ClickHouse } = require('clickhouse');
const clickhouse = new ClickHouse({
    url: process.env.DB_HOST,
    port: process.env.DB_PORT,
    debug: false,
    basicAuth: {
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
    },
    isUseGzip: true,
    trimQuery: false,
    usePost: false,
    UseSession: false,
    format: "json", // "json" || "csv" || "tsv"
    raw: true,
    config: {
        //session_id: 'session_id if neeed',          
        //session_timeout: 120,
        output_format_json_quote_64bit_integers: 0,
        enable_http_compression: 0,
        database: process.env.DB_NAME,
    },
    reqParams: {

    }
});

module.exports = {
    initFuncionTracking: async function (sysTracking) {
        var dictsetting ={}; 
        let state=0;
        try {
            
                console.log("[x] start setting tracking with :%",sysTracking, ", in date:", new Date().toLocaleTimeString());
                var query = "select * from fmis.SettingTracking where tracking='"+sysTracking+"'";
                const rows = await clickhouse.query(query).toPromise();                 
                if (rows!=undefined) {
                    state=1;
                    const dtjson = JSON.parse(rows);
                                 
                    dtjson.data.forEach(element => {
                        dictsetting[element.function] = element;
                    });      
                }                          
                return dictsetting;         

        } catch  {
            //console.log("error:", error);
        }
        finally 
        {
          if (state!=0) {
            console.log("data get clickhouse table settingtracking:");
          }
          else
          {
          console.log("data read of file settingtracking:");
          setting.settingtracking.forEach(element => {
            dictsetting[element.function] = element;    
          });         
          }
          return dictsetting;
        }
    }    
};