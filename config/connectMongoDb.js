
require('dotenv').config();
const setting = require('./environment.js');
const { MongoClient } = require('mongodb');
// Connection URL
const url = process.env.DATABASE_URI;
const client = new MongoClient(url);
const dbName = process.env.DATABASE_MONGO;
var { transform } = require("node-json-transform");


module.exports = {
  getConfigAll: async function (dictSettingTracking) {
    var dictconfig = {};
    try {
      const nameField1 = JSON.parse(dictSettingTracking["getConfig"].name_field);
      const nameField2 = JSON.parse(dictSettingTracking["getConfigDefault"].name_field);
      const map1 = {
        item: {
          sysId: nameField1.key,
          event: nameField1.value
        },
      };
      const map2 = {
        item: {
          sysId: nameField2.key,
          event: nameField2.value
        },
      };
      await client.connect();
      console.log('Connected successfully to server');

      var collec1 = dictSettingTracking["getConfig"].name_table;
      var collec2 = dictSettingTracking["getConfigDefault"].name_table;;
      await Promise.all([getConfig(collec1), getConfigDefault(collec2)]).then((value) => {
        var result1 = transform(value[0], map1);
        var result2 = transform(value[1], map2);
        result1.forEach(element => {
          if (element.event != undefined) {
            dictconfig[element.sysId] = element.event;
          }
          else {
            dictconfig[element.sysId] = result2[0].event;
          }
        })
      });
      //return dictconfig;
    } catch (error) {
      console.log("error:", error);
    }
    finally {
      if (dictconfig != undefined) {
        console.log("data get clickhouse table settingtracking:");
      }
      else {
        console.log("data read of file environment.js::");
        dictconfig[setting.sysId.name] = setting.event;
      }
      return dictconfig;
    }
  }
}
async function getConfig(collection) {
  try {
    const db = client.db(dbName);
    var filteredDocs1 = await db.collection(collection).find({}).toArray();
    return new Promise((resolve, reject) => {
      resolve(filteredDocs1);
    });
  } catch (error) {
    console.log("error:", error);
  }
}
async function getConfigDefault(collection) {
  try {
    const db = client.db(dbName);
    var filteredDocs1 = await db.collection(collection).find({}).toArray();
    return new Promise((resolve, reject) => {
      resolve(filteredDocs1);
    });
  } catch (error) {
    console.log("error:", error);
  }
}