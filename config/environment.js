module.exports = {
    "settingtracking": [
        {
            "tracking": "BUS",
            "function": "getConfig",
            "name_type": "mongoDB",
            "database": "fmisdb-master-demo",
            "name_table": "tenant",
            "name_field": { "key": "sysId", "value": "environment.event" },
            "update_time": "2022-08-09"
        },
        {
            "tracking": "BUS",
            "function": "getConfigDefault",
            "name_type": "mongoDB",
            "database": "fmisdb-master-demo",
            "name_table": "environment",
            "name_field": { "key": "sysId", "value": "event" },
            "update_time": "2022-08-09"
        },
        {
            "tracking": "BUS",
            "function": "getSysId",
            "name_type": "clickhouse",
            "database": "fmis", "name_table": "Mapping",
            "name_field": { "key": "sysId", "value": "device_code" },
            "update_time": "2022-08-09"
        },
        {
            "tracking": "BUS",
            "function": "initDictData",
            "name_type": "clickhouse",
            "database": "fmis",
            'name_table': "v_WayPoint_Lastseen",
            "name_field": { "key": "device_code", "value": { "timestamp": "timestamp", "lat": "lat", "lng": "lng", "speed": "speed" } },
            "update_time": "2022-08-09"
        }
    ],
    "sysId": {
        "name":"VBD"
    },
    "event": {
        "stop": {
            "name": "STOP",
            "source": 16000,
            "typeId": 16001,		// type of the event
            "waitingTime": 300000,		//30000 ms -> 5 minutes -> 300s  
            "distance": 100		// 0.1 km ->100 m
        },
        "idle": {
            "name": "IDLE",
            "source": 16000,
            "typeId": 16002,
            "speed": 0.28,		// Speed limit 0.28m/s ~ 1km/h
            "numberOfIDLESignal": 3,	//Device continuously sends IDLE status with speed <= 1 km/h at least 10 times
            "numberOfRunningSignal": 3, //Device starts and continuously sends IDLE status with speed > 1km/h at least 10 times
            "distance": 100		// 0.1 km ->100 m
        }
    },
    "myCache": {
        "alldata_expire":3600,
        "register_expire": 120,
        "stopidle_expire": 60
    }

}
