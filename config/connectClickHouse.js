'use strict';
require('dotenv').config();
const setting = require('./environment.js');
const NodeCache = require("node-cache");
const myCache = new NodeCache({ checkperiod: setting.myCache.alldata_expire });// will check every 60 minute
const { ClickHouse } = require('clickhouse');
const clickhouse = new ClickHouse({
    url: process.env.DB_HOST,
    port: process.env.DB_PORT,
    debug: false,
    basicAuth: {
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
    },
    isUseGzip: true,
    trimQuery: false,
    usePost: false,
    UseSession: false,
    format: "json", // "json" || "csv" || "tsv"
    raw: true,
    config: {
        //session_id: 'session_id if neeed',          
        //session_timeout: 120,
        output_format_json_quote_64bit_integers: 0,
        enable_http_compression: 0,
        database: process.env.DB_NAME,
    },
    reqParams: {

    }
});
var { transform } = require("node-json-transform");


module.exports = {
    getSysId: async function (dictSettingTracking,deviceId) {
        try {
            var nameField={};           
            if (dictSettingTracking["getSysId"].name_field.key!=undefined) {
                nameField=dictSettingTracking["getSysId"].name_field;
            }
            else  nameField=JSON.parse(dictSettingTracking["getSysId"].name_field);
            
            const map1 = {
                item: {
                    device_code: nameField.value,
                    sysId: nameField.key
                },
            };
            let tokens = myCache.get('register');
            if (tokens == null) {
                console.log("[x] cache token register was expired at:", new Date().toLocaleTimeString());
                var query = "SELECT " + nameField.value + " , " + nameField.key + " FROM " + dictSettingTracking["getSysId"].database + "." + dictSettingTracking["getSysId"].name_table;
                const rows = await clickhouse.query(query).toPromise();
                const dtjson = JSON.parse(rows);
                var configSysId = {};
                var result1 = transform(dtjson.data, map1);
                result1.forEach(element => {
                    configSysId[element.device_code] = element.sysId;
                });
                myCache.set('register', configSysId, setting.myCache.register_expire); // expire_in 2 minutes   
                return configSysId[deviceId];
            }
            else {

                return tokens[deviceId];
            }

        } catch (error) {
            console.log("error:", error);
        }
    },
    initDictData: async function (dictSettingTracking,dictdataStop, dictdataIdle) {
        try {
            const nameField=JSON.parse(dictSettingTracking["initDictData"].name_field);
            const map2 = {
                item: {
                    device_code: nameField.key,
                    timestamp: nameField.value.timestamp,
                    lat: nameField.value.lat,
                    lng: nameField.value.lng,
                    speed: nameField.value.speed
                },
            };
            var query = "SELECT "+ nameField.key+","+ nameField.value.timestamp +","+nameField.value.lat +","+nameField.value.lng+","+nameField.value.speed+" FROM "+dictSettingTracking["initDictData"].database +"."+dictSettingTracking["initDictData"].name_table+" ";
            const rows = await clickhouse.query(query).toPromise();
            const dtjson = JSON.parse(rows);
            var result1 = transform(dtjson.data, map2);
            result1.forEach(element => {
                dictdataStop[element.device_code] = element;
                dictdataIdle[element.device_code] = element;
            });

        } catch (error) {
            console.log("error:", error);
        }
    },    
    insertData: async function(queryinsert,rows)
    {        
        return await new Promise((resolve, reject) => {
          const kq= clickhouse.insert(queryinsert,rows).toPromise()
            resolve(kq);
        }).catch(error =>{
            console.log(error); 
        });
    }
};