FROM node:latest
WORKDIR /fmis-tracking-main
COPY . .
RUN npm install
EXPOSE 9000
CMD ["node", "./service/service_find_StopIdle/findStopIdle.js"]